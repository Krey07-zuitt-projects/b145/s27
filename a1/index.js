

db.fruits.aggregate([
	{
		$match:{"onSale":true}
	},
	{
		$group:{
			_id:"$onSale",
			fruitsOnSale:{
				$sum:1
			}
		}
	},{
            $project:{"_id":0}}

	]);

db.fruits.aggregate([
	{
		$match:{"stock":{$gt:20}}
	},
	{
		$group: 
		{
			_id: "enoughStock",
			"total": {$sum: 1 }
		} 
	}
	]);

db.fruits.aggregate([
	{$match: {"onSale": true} },{
		$unwind:"$origin"
	},
	{
       $group:
         {
           _id: "$origin",
         
           avgPrice: { $avg: "$price" }
         }
     }

	]);

db.fruits.aggregate([
	{
		$unwind:"$origin"
	},
	{
       $group:
         {
           _id: "$origin",
         
           maxPrice: { $max: "$price" }
         }
     }

	]);

db.fruits.aggregate([
	{
		$unwind:"$origin"
	},
	{
       $group:
         {
           _id: "$origin",
         
           minPrice: { $min: "$price" }
         }
     }

	]);
